package base;

import static org.junit.Assert.*;

import org.junit.Test;

public class DominoTest {

	 @Test
	    public void testDominoPlacement() {
	        Domino domino = new Domino(3, 4);
	        domino.place(1, 2, 3, 4);
	        assertEquals("[34](2,3)(4,5)", domino.toString());
	    }

	    @Test
	    public void testDominoInversion() {
	        Domino domino = new Domino(3, 4);
	        domino.place(1, 2, 3, 4);
	        domino.invert();
	        assertEquals("[34](4,5)(2,3)", domino.toString());
	    }
	    @Test
	    public void testToStringUnplaced() {
	        Domino domino = new Domino(3, 4);
	        assertEquals("[34]unplaced", domino.toString());
	    }

	    @Test
	    public void testToStringPlaced() {
	        Domino domino = new Domino(3, 4);
	        domino.place(1, 2, 3, 4);
	        assertEquals("[34](2,3)(4,5)", domino.toString());
	    }

	    @Test
	    public void testAppendCoordinate() {
	        Domino domino = new Domino(3, 4);
	        StringBuilder result = new StringBuilder();
	        domino.appendCoordinate(result, 1, 2);
	        assertEquals("(2,3)", result.toString());
	    }
}

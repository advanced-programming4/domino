	package base;
	/**
	 * @author Kevan Buckley, maintained by __student
	 * @version 2.0, 2014
	 */
	
	public class Domino implements Comparable<Domino> {
	    private int high;
	    private int low;
	    private int hx;
	    private int hy;
	    private int lx;
	    private int ly;
	    private boolean placed = false;

	    // Getter methods for encapsulation
	    public int getHigh() {
	        return high;
	    }

	    public int getLow() {
	        return low;
	    }
	    public int getHy() {
	        return hy;
	    }
	    public int getHx() {
	        return hx;
	    }
	    public int getLx() {
	        return lx;
	    }
	    public int getLy() {
	        return ly;
	    }
	
	  public Domino(int high, int low) {
	    super();
	    this.high = high;
	    this.low = low;
	  }
	  
	  public void place(int hx, int hy, int lx, int ly) {
	    this.hx = hx;
	    this.hy = hy;
	    this.lx = lx;
	    this.ly = ly;
	    placed = true;
	  }
	
	    private static final String UNPLACED = "unplaced";

	  public String toString() {
		    StringBuilder result = new StringBuilder();
		    result.append("[").append(high).append(low).append("]");

		    if (!placed) {
		        result.append(UNPLACED);
		    } else {
		        appendCoordinate(result, hx, hy);
		        appendCoordinate(result, lx, ly);
		    }

		    return result.toString();
		}

		void appendCoordinate(StringBuilder result, int x, int y) {
		    result.append("(").append(x + 1).append(",").append(y + 1).append(")");
		}
	
	  /** turn the domino around 180 degrees clockwise*/
	  
	  public void invert() {
	    int tx = hx;
	    hx = lx;
	    lx = tx;
	    
	    int ty = hy;
	    hy = ly;
	    ly = ty;    
	  }
	
	  public boolean ishl() {    
	    return hy==ly;
	  }
	
	
	  public int compareTo(Domino arg0) {
	    if(this.high < arg0.high){
	      return 1;
	    }
	    return this.low - arg0.low;
	  }
	  
	  
	  
	}

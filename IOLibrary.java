package base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;

public final class IOLibrary {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private IOLibrary() {
        // Private constructor to prevent instantiation
    }

    public static String getString() {
        return readInput("Error reading string. Please try again.", IOLibrary::readLine);
    }

    public static InetAddress getIPAddress() {
        return readInput("Error reading IP address. Please try again.", IOLibrary::parseIPAddress);
    }

    private static String readLine() throws IOException {
        return reader.readLine();
    }

    private static InetAddress parseIPAddress() throws IOException {
        String[] chunks = reader.readLine().split("\\.");
        byte[] data = { Byte.parseByte(chunks[0]), Byte.parseByte(chunks[1]), Byte.parseByte(chunks[2]),
                Byte.parseByte(chunks[3])};
        return Inet4Address.getByAddress(data);
    }

    private static <T> T readInput(String errorMessage, InputReader<T> inputReader) {
        do {
            try {
                return inputReader.read();
            } catch (IOException e) {
                // Handle or log the exception
                System.err.println(errorMessage);
            }
        } while (true);
    }

    private interface InputReader<T> {
        T read() throws IOException;
    }
}

package base;

import static org.junit.Assert.*;

import org.junit.Test;

public class SpaceLocationTest {

	@Test
    public void testSpaceLocationToString() {
        SpaceLocation location = new SpaceLocation(1, 2, SpaceLocation.DIRECTION.HORIZONTAL);

        assertEquals("(3,2,HORIZONTAL)", location.toString());
    }

}

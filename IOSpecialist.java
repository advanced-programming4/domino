  	package base;
  	
  	/**
  	 * @author Kevan Buckley, maintained by __student
  	 * @version 2.0, 2014
  	 */
  	
  	public class IOSpecialist {
  		private final IOLibrary ioLibrary;

  	    public IOSpecialist(IOLibrary ioLibrary) {
  	        this.ioLibrary = ioLibrary;
  	    }

  	    public String getString() {
  	        return IOLibrary.getString();
  	    }

		public IOLibrary getIoLibrary() {
			return ioLibrary;
		}
  	}
  	
  	    

package base;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MainTest {

	private Main main;

	@Before
    public void setUp() {
        main = new Main();
    }
     
	@Test
    public void testGenerateGuesses() {
        main.generateGuesses();
        assertNotNull(main._g);
        assertEquals(28, main._g.size());  
    }
	
	
}

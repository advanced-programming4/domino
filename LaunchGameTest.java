package base;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

public class LaunchGameTest {

	@Test
    public void testGetIpa() throws UnknownHostException {
        InetAddress expectedInetAddress = InetAddress.getByName("localhost");
        LaunchGame launchGame = new LaunchGame(expectedInetAddress);

        InetAddress actualInetAddress = launchGame.getIpa();

        assertNotNull(actualInetAddress);
        assertEquals(expectedInetAddress, actualInetAddress);
    }
}

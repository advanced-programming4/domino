package base;

import static org.junit.Assert.*;

import org.junit.Test;

public class MultiLingualStringTableTest {

	@Test
    public void testGetMessageInEnglish() {
        assertEquals("Enter your name:", MultiLingualStringTable.getMessage(0));
        assertEquals("Welcome", MultiLingualStringTable.getMessage(1));
        assertEquals("Have a good time playing Abominodo", MultiLingualStringTable.getMessage(2));
    }


    @Test
    public void testGetMessageOutOfBounds() {
        // Trying to access an index that is out of bounds should result in an exception
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> MultiLingualStringTable.getMessage(3));
    }
}
